# FERRY-AUTH

## Install Notes
 - Add user data source for dsHandler config:
   ```
        "user": "postgres://user:pass@host:5432/user"
   ```
 - Add plugin in plugin-list.js:
   ```
        'ferry-auth'     : require('ferry-auth'),
   ```
 - For each route, in the plugin's  entrypoints.js, add config.auth = 'ferry-token':
   ```
        {
           method : 'POST',
           path   : '/user/{id}/subscriptions',
           handler: require('./entrypoints/userSubscriptions'),
           config : {
               auth: 'ferry-token'
           }
        }
   ```


## Usage

The current logged in use is available in ```request.auth.credentials```

